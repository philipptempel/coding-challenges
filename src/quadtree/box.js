class Box {

  constructor(cap, pos, dims) {
    this.capacity = cap;
    this.position = pos;
    this.dimensions = dims;
    this.data = [];
    this.children = [];
  }

  contains(point) {
    let w = this.w;
    let h = this.h;
    let x = this.x;
    let y = this.y;
    let px = point.x
    let py = point.y;

    return x - w / 2 <= px && px <= x + w / 2 && y - h / 2 <= py && py <= y + h / 2;
  }

  get northeast() {
    return this.children[3];
  }

  get northwest() {
    return this.children[2];
  }

  get southwest() {
    return this.children[1];
  }

  get southeast() {
    return this.children[0];
  }

  get subdivided() {
    return this.children.length > 0;
  }

  add(point) {
    if (!this.contains(point)) {
      return false;
    }

    // If the tree is subdivided ...
    if (this.subdivided) {
      // loop over all children and add it
      for (let child of this.children) {
        child.add(point);
      }
    } else {
      if (this.data.length == this.capacity) {
        this.subdivide(point);
      } else {
        this.data.push(point);
      }
    }
  }

  subdivide(point) {
    let dims = this.dimensions.copy().mult(0.5);
    let ang = atan2(dims.y, dims.x);

    // Create the children
    for (let i = 0; i < 4; i++) {
      this.children.push(new Box(this.capacity, this.position.copy().add(dims.copy().mult(0.5).rotate(i * HALF_PI)), dims));
    }

    // Move all our data points into the children
    for (let p of this.data) {
      for (let child of this.children) {
        child.add(p);
      }
    }
    // Remove all our data, it's now contained in the
    // childrens' data array
    this.data = [];

    // Add the new point to the matching child
    for (let child of this.children) {
      child.add(point);
    }
  }

  get x() {
    return this.position.x;
  }

  get y() {
    return this.position.y;
  }

  get halfdimensions() {
    return this.dimensions.copy().mult(0.5);
  }

  get w() {
    return this.dimensions.x;
  }

  get halfw() {
    return this.w / 2;
  }

  get h() {
    return this.dimensions.y;
  }

  get halfh() {
    return this.h / 2;
  }

  draw() {
    push();
    translate(this.x, this.y);

    noFill();
    stroke(255, 0, 255);

    rectMode(CENTER)
    rect(0, 0, this.w, this.h);

    strokeWeight(1);
    point(0, 0);

    pop();

    // Plot data points
    for (let data of this.data) {
      circle(data.x, data.y, 2 * 2);
    }

    // Plot all children
    for (let child of this.children) {
      child.draw();
    }
  }

}
