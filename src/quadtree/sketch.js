let tree;

function setup() {
  createCanvas(400, 400);
  
  tree = new Box(4, createVector(width/2, height/2), createVector(width, height));
}

function mousePressed() {
  tree.add(createVector(mouseX, mouseY));
}

function draw() {
  background(61);
  tree.draw();
}