let pile;
let size = [];
let c;
let PCOLORS = [];
var gui;

var frs = [];
var pFrameRate;
var seedSize = 1;
var topplePerFrame = 1;

function setup() {
  pixelDensity(4);
  createCanvas(400, 400);

  size = [100, 100];
  pile = new Pile(size);

  pFrameRate = createP(frameRate()).style('color: white;');

  PCOLORS = [color('#D94E8F'), color('#D7D7D9'), color('#D2D911'), color('#F2BE22'), color('#A6290D')];
  
  makeGui();
}

function makeGui() {
  gui = createGui('Control');
  gui.setPosition(0.5 * width + 2, height + 2);
  sliderRange(-1, 7, 0.2);
  gui.addGlobals('seedSize');
  sliderRange(1, 250, 1);
  gui.addGlobals('topplePerFrame');
}

function mouseClicked() {
  let x = mouseX;
  let y = mouseY;

  if (0 < x && x < width && 0 < y && y < height) {
    let grains = floor(pow(10, seedSize));
    
    pile.add(floor(x / width * pile.width), floor(y / height * pile.height), grains);
    createP('Added ' + grains + ' pieces of grain').style('color: white;');
  }
}

function updateFrameRate() {
  if (frs.length > 30) {
    frs.splice(0, 1);
  }
  frs.push(frameRate());

  let f = 0;
  for (let fr of frs) {
    f += fr;
  }
  f /= frs.length;
  
  pFrameRate.html(round(f) + '&nbsp;fps');
}

function draw() {
  pile.draw();
  for ( let i = 0; i < topplePerFrame; i++ ){
    pile.update();
  }
  updateFrameRate();
}