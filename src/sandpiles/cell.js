class Cell {
  constructor(ij, dims, cnt) {
    this.index = createVector(...ij);
    this.dimensions = createVector(...dims);
    this.count = cnt || 0;
    this.newcount = 0;
    this.i = this.index.x;
    this.j = this.index.y;
  }

  /*
  get i() {
    return this.index.x;
  }
  */

  /*
  get j() {
    return this.index.y;
  }
  */

  get w() {
    return this.dimensions.x;
  }

  get h() {
    return this.dimensions.y;
  }

  get x() {
    return (this.j + 0.5) * this.w;
  }

  get y() {
    return (this.i + 0.5) * this.h;
  }

  clone() {
    return new Cell([this.i, this.j], [this.w, this.h], this.count);
  }

  update() {
    this.count = this.newcount;
    this.newcount = 0;
  }

  draw() {
    let idx;

    let i = this.i;
    let j = this.j;
    let w = this.w;
    let h = this.h;

    colorMode(HSB, 360, 4, 100, 100);
    let clr = color(360, this.count, 100, 100);

    for (let x = 0; x < w; x++) {
      for (let y = 0; y < h; y++) {
        idx = ((j * w + x) + (i * h + y) * width) * 4;
        pixels[idx + 0] = red(clr);
        pixels[idx + 1] = green(clr);
        pixels[idx + 2] = blue(clr);
        pixels[idx + 3] = alpha(clr);
      }
    }
  }
}
