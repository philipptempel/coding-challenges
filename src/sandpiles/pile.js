class Pile {

  constructor(sz) {
    this.size = sz;
    this.canvas = createGraphics(this.width, this.height);
    this.canvas.pixelDensity(pixelDensity());
    this.pile = (new Array(this.length)).fill(0);
  }

  get width() {
    return this.size[1];
  }

  get height() {
    return this.size[0];
  }

  get length() {
    return this.width * this.height;
  }

  get random() {
    return floor(random(this.length));
  }

  add(j, i, c) {
    if (c === undefined) {
      this.pile[j] += floor(i);
    } else {
      // Pixels given?
      if ( j > this.width ) {
        j = j % this.width;
        i = i % this.height;
      }
      this.pile[j + i * this.width] += floor(c);
    }
  }

  update() {
    let idx;

    let w = this.width;
    let h = this.height;
    let next = (new Array(this.length)).fill(0);

    // Copy count
    for (let i = 0; i < h; i++) {
      for (let j = 0; j < w; j++) {
        idx = this.sub2ind(i, j);

        next[idx] = this.pile[idx];
      }
    }

    // Topple
    for (let i = 0; i < h; i++) {
      for (let j = 0; j < w; j++) {
        idx = this.sub2ind(i, j);

        if (this.pile[idx] >= 4) {
          next[idx] -= 4;
          for (let nij of this.neighbors(i, j)) {
            next[nij] += 1;
          }
        }
      }
    }

    // Ensure not below zero
    for (let i = 0; i < h; i++) {
      for (let j = 0; j < w; j++) {
        idx = this.sub2ind(i, j);

        next[idx] = max(0, next[idx]);
      }
    }

    this.pile = next;
  }

  neighbors(i, j) {
    let n = [];

    if (j === undefined) {
      [i, j] = this.ind2sub(i);
    }

    let ijs = [
      [+1, 0],
      [0, +1],
      [-1, 0],
      [0, -1]
    ];
    let ni, nj;

    for (let ij of ijs) {
      ni = i + ij[0];
      nj = j + ij[1];

      if (0 <= ni && ni < this.height && 0 <= nj && nj < this.width) {
        n.push(this.sub2ind(ni, nj));
      }
    }

    return n;
  }

  sub2ind(i, j) {
    return sub2ind(this.size, i, j);
  }

  ind2sub(ind) {
    return ind2sub(this.size, ind);
  }

  draw() {
    let c = this.canvas;
    let h = this.height;
    let w = this.width;
    let pile = this.pile;
    let d = c.pixelDensity();

    let r, g, b, a;
    let pidx, cidx, cnt;
    let index;
    let ang, clr;

    c.loadPixels();

    for (let i = 0; i < h; i++) {
      for (let j = 0; j < w; j++) {
        cnt = pile[this.sub2ind(i, j)];
        clr = PCOLORS[min(cnt, PCOLORS.length - 1)];

        r = red(clr);
        g = green(clr);
        b = blue(clr);
        a = alpha(clr);

        // Fill subpixels
        for (let di = 0; di < d; di++) {
          for (let dj = 0; dj < d; dj++) {
            index = 4 * ((i * d + dj) * c.width * d + (j * d + di));
            c.pixels[index + 0] = r;
            c.pixels[index + 1] = g;
            c.pixels[index + 2] = b;
            c.pixels[index + 3] = 255;
          }
        }
      }
    }

    c.updatePixels();
    image(this.canvas, 0, 0, width, height);
  }

}
