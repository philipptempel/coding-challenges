let params = {
  a: 1,
  b: 1,
  delta: 1,
  // Draws per frame
  perFrame: 10,
  perFrameMin: 1,
  perFrameMax: 100,
  perFrameStep: 1
}

let t = 0;
let curves = [];

let pFrameRate;

function setup() {
  pixelDensity(2);
  createCanvas(windowWidth, windowHeight);
  background(31);
  frameRate(30);

  params.delta = 0.01;

  let rows = 6;
  let cols = 6;
  let w = width / cols;
  let h = height / rows;
  let r = min(w, h) / 4;

  for (let y = 0; y < rows; y++) {
    for (let x = 0; x < cols; x++) {
      curves.push(new LissajousCurve(1 + x, 1 + y, HALF_PI, [(x + 1 / 2) * w, (y + 1 / 2) * h], r))
    }
  }
}

function draw() {
  background(31);
  stroke(255);

  if (abs(t) > TWO_PI) {
    for (let curve of curves) {
      curve.reset();
    }
    t = 0;
    clear();
  }

  for (let i = 0; i < params.perFrame; i++) {
    for (let curve of curves) {
      curve.update();
      curve.draw();
    }

    t -= params.delta;
  }
}
