class LissajousCurve {

  constructor(a, b, d, center, r) {
    this.center = createVector(...center);
    this.a = a;
    this.b = b;
    this.d = d;
    this.radius = r || 1;
    this.points = [];
    this.update();
  }

  get x() {
    return this.cx + this.radius * sin(this.a * t + this.d);
  }

  get cx() {
    return this.center.x;
  }

  get y() {
    return this.cy + this.radius * sin(this.b * t);
  }

  get cy() {
    return this.center.y;
  }
  
  get head() {
    return this.points[this.points.length - 1];
  }

  reset() {
    this.points = [];
  }

  update() {
    this.points.push(createVector(this.x, this.y));
  }

  draw() {
    stroke(255);
    noFill();
    beginShape();
    strokeWeight(1);
    for (let point of this.points) {
      vertex(point.x, point.y);
    }
    endShape();
    strokeWeight(4);
    point(this.head.x, this.head.y);
  }

}