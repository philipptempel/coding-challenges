let rs, rm, rh;
let colors = [];

function setup() {
  pixelDensity(4);

  createCanvas(200, 400);

  angleMode(DEGREES);
  let magenta = '#480A59';
  let pink = '#8C116B';
  let darkblue = '#20448C';
  let orange = '#F2BC1B';
  colors = {
    'hour': color(orange),
    'minute': color(darkblue),
    'second': color(pink)
  };
  
  frameRate(5);

}

function draw() {
  background(31);

  let h = 1 / 3 * height;
  let w = 1 / 3 * width;

  // Hours
  push();
  translate(2 * w, 3 * h);
  rotate(-90);
  scale(height, w);
  drawHours(h);
  pop();

  // Minutes
  push();
  translate(1 * w, 3 * h);
  rotate(-90);
  scale(height, w);
  drawMinutes(h);
  pop();

  // Seconds
  push();
  translate(0 * w, 3 * h);
  rotate(-90);
  scale(height, w);
  drawSeconds(h);
  pop();

}

function drawSeconds(hght) {
  let sec = second();
  
  let clr = colors.second;
  
  let h = floor(hue(clr));
  let s = floor(saturation(clr));
  let b = floor(brightness(clr));
  let a = floor(alpha(clr));
  
  colorMode(HSB);
  noStroke();
  for ( let i = 0; i < sec; i++ ) {
    fill(h, s, b * (i + 1) / 60, a);
    rect(i / 60, 0, 1 / 60, 1);
  }
}

function drawMinutes(hght) {
  let m = minute();
  
  let clr = colors.minute;
  
  let h = floor(hue(clr));
  let s = floor(saturation(clr));
  let b = floor(brightness(clr));
  let a = floor(alpha(clr));
  
  colorMode(HSB);
  noStroke();
  for ( let i = 0; i < m; i++ ) {
    fill(h, s, b * (i + 1) / 60, a);
    rect(i / 60, 0, 1 / 60, 1);
  }
}

function drawHours(hght) {
  let hr = hour();
  let H = hr % 12;
  let clr = colors.hour;
  
  let h = floor(hue(clr));
  let s = floor(saturation(clr));
  let b = floor(brightness(clr));
  let a = floor(alpha(clr));
  
  colorMode(HSB);
  noStroke();
  for ( let i = 0; i < H; i++ ) {
    fill(h, s, b * (i + 1) / H, a);
    rect(i / 12, 0, 1 / 12, 1);
  }
}
