let x = 0,
  y = 0;
let perFrame = 500;

function setup() {
  pixelDensity(4);
  createCanvas(800, 800);
  background(31);
  x = 0;
  y = 0;
}

function turtleMove() {
  let xn, xy;

  let r = random(1);

  if (r < 0.01) {
    xn = 0 * x;
    yn = 0.16 * y;
  } else if (r < 0.86) {
    xn = 0.85 * x + 0.04 * y + 0.00;
    yn = -0.04 * x + 0.85 * y + 1.60;
  } else if (r < 0.93) {
    xn = 0.20 * x - 0.26 * y + 0.00;
    yn = 0.23 * x + 0.22 * y + 1.60;
  } else {
    xn = -0.15 * x + 0.28 * y + 0.00;
    yn = 0.26 * x + 0.24 * y + 0.44;
  }

  x = xn;
  y = yn;
}

function turtleDraw() {
  stroke(34, 139, 34);
  strokeWeight(1);
  let px = map(x, -2.6558, 2.6558, 0, width);
  let py = map(y, 0, 9.9983, height, 0);
  point(px, py);
}

function draw() {
  for (let i = 0; i < perFrame; i++) {
    turtleDraw();
    turtleMove();
  }
}
