let cells = [];

function setup() {
  createCanvas(windowWidth, windowHeight);

  // Initialize canvas with a few cells
  for (let i = 0; i < 5; i++) {
    cells.push(new Cell(
        createVector(random(10, width - 10), random(10, height - 10))
    ))
  }
}

function mousePressed() {
  let cell;

  // Loop backwards over all cells and check if they splice
  for (let i = cells.length - 1; i >= 0; i--) {
    cell = cells[i];

    if (cell.near(mouseX, mouseY)) {
      cells.push(...cell.mitosis());
      cells.splice(i, 1);
    }
  }

  return false;
}

function draw() {
  background(31);

  let cell;
  // Loop over each cell
  for (let i = cells.length - 1; i >= 0; i--) {
    // Get cell
    cell = cells[i];
    // If cell is dead ...
    if (cell.dead()) {
      // ... remove it from the stack
      cells.splice(i, 1)
    } else {
      // Update cell
      cell.update();
      // Draw cell
      cell.draw();
    }
  }
}
