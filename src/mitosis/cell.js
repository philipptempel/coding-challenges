class Cell {

  constructor(pos, radius) {
    this.growrate = 0.05;
    this.minRadius = 5;
    this.maxRadius = 40;
    this.position = pos || createVector(0, 0);
    this.velocity = p5.Vector.random2D();
    this.radius = radius || this.minRadius;
    // Used only for the cell's noise
    this.z = random(0, width);
    // When the cell was born
    this.birth = millis();
    // Cell's die after so much time (in seconds)
    this.lifespan = 300 + random(-50, 50);
  }

  get x() {
    return this.position.x;
  }

  get y() {
    return this.position.y;
  }

  get z() {
    return this.position.z;
  }

  get vx() {
    return this.velocity.x;
  }

  get vy() {
    return this.velocity.y;
  }

  set x(v) {
    this.position.x = v;
  }

  set y(v) {
    this.position.y = v;
  }

  set z(v) {
    this.position.z = v;
  }

  update() {
    this.move();
    this.grow();
    this.edges();
  }

  move() {
    this.position.add(this.velocity)
    let ang = map(
        noise(
            map(this.position.x, 0, width, -1, 1),
            map(this.position.y, 0, height, -2, 2),
            this.position.z
        ),
        0,
        1,
        -TWO_PI * 2,
        TWO_PI * 2
        );
    this.velocity = p5.Vector.fromAngle(
        ang,
        random(0.1, 1.0)
    )
    this.z += random(-0.5, +0.5);
  }

  grow() {
    if (this.radius < this.maxRadius) {
      this.radius += this.growrate;
    }
  }

  edges() {
    // Check X-coordinate
    if (this.x < 0) {
      this.x = width;
    } else if (width < this.x) {
      this.x = 0;
    }

    // Check Y-coordinate
    if (this.y < 0) {
      this.y = height;
    } else if (height < this.y) {
      this.y = 0;
    }
  }

  mitosis() {
    let splitDirn = p5.Vector.random2D();
    let newRadius = max(this.minRadius, this.radius * this.growrate);

    return [
      new Cell(this.position.copy().add(splitDirn), newRadius),
      new Cell(this.position.copy().sub(splitDirn), newRadius)
    ];
  }

  near(x, y) {
    return dist(this.x, this.y, x, y || 0) <= this.radius;
  }

  dead() {
    return this.birth + this.lifespan * 1000 <= millis();
  }

  draw() {
    push();
    translate(this.x, this.y);
    fill(255, 100);
    strokeWeight(0.5);
    circle(0, 0, this.radius);
    pop();
  }

}
