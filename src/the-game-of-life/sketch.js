let size = [20, 20];

let dims;
let grid;
let cnvs;
let paused = true;
let w, h;

function setup() {
  pixelDensity(4);
  createCanvas(400, 400);

  w = size[1];
  h = size[0];

  cnvs = createGraphics(w, h).pixelDensity(pixelDensity());

  createButton('Start').mousePressed(() => {
    paused = false;
  });
  createButton('Pause/Stop').mousePressed(() => {
    paused = true;
  });

  grid = (new Array(w * h)).fill(0);
  frameRate(5);

  grid[sub2ind(size, 0, 1)] = 1;
  grid[sub2ind(size, 1, 0)] = 1;
  grid[sub2ind(size, 1, 2)] = 1;
  grid[sub2ind(size, 2, 0)] = 1;
  grid[sub2ind(size, 2, 2)] = 1;
  grid[sub2ind(size, 3, 1)] = 1;
}

function mousePressed() {
  let mx = mouseX,
    my = mouseY;

  if (0 <= mx && mx < width && 0 <= my && my < height) {
    let r = floor(my / height * h);
    let c = floor(mx / width * w);
    let idx = sub2ind(size, r, c);
    
    grid[idx] = grid[idx] ? 0 : 1;
  }
}

function neighbors(i, j) {
  let ijs = [
    [+1, +0],
    [+1, +1],
    [+0, +1],
    [-1, +1],
    [-1, +0],
    [-1, -1],
    [+0, -1],
    [+1, -1]
  ];

  let ni, nj;
  let n = [];
  let c = cols(size),
    r = rows(size);

  for (let ij of ijs) {
    ni = i + ij[0];
    nj = j + ij[1];
    
    if (0 <= ni && ni < r && 0 <= nj && nj < c) {
      n.push(sub2ind(size, ni, nj));
    }
  }

  return n;
}

function update() {
  if (paused) {
    return;
  }

  let ngrid = (new Array(w * h)).fill(0);
  let ncount, ij;

  for (let idx = 0; idx < grid.length; idx++) {
    ngrid[idx] = 0;

    ncount = 0;
    ij = ind2sub(size, idx);

    for (let n of neighbors(ij[0], ij[1])) {
      ncount += grid[n] ? 1 : 0;
    }
    
    // Live cell
    if ( grid[idx] == 1  ){
      if ( ncount == 2 || ncount == 3 ) {
        ngrid[idx] = 1;
      } else {
        ngrid[idx] = 0;
      }
    // Dead cell
    } else {
      if ( ncount == 3 ) {
        ngrid[idx] = 1;
      } else {
        ngrid[idx] = 0;
      }
    }
  }

  grid = ngrid;
}

function draw() {
  background(220);

  let i, j;
  let r = rows(size),
    c = cols(size);
  let d = cnvs.pixelDensity();

  cnvs.loadPixels();
  let pidx, clr;
  loadPixels();
  for (let y = 0; y < r; y++) {
    for (let x = 0; x < c; x++) {
      clr = grid[sub2ind(size, y, x)] ? 255 : 0;

      for (let i = 0; i < d; i++) {
        for (let j = 0; j < d; j++) {
          // loop over
          idx = 4 * ((y * d + j) * c * d + (x * d + i));
          cnvs.pixels[idx + 0] = clr;
          cnvs.pixels[idx + 1] = clr;
          cnvs.pixels[idx + 2] = clr;
          cnvs.pixels[idx + 3] = 100;
        }
      }
    }
  }

  cnvs.updatePixels();
  image(cnvs, 0, 0, width, height);

  update();

}