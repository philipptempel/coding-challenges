let sliders = [];
let rmax;
let pd;
let tooltips = [];

let img;

function setup() {
  pd = 1;
  pixelDensity(pd);

  createCanvas(400, 400);

  rmax = 0.98 * min(width / 2, height / 2);

  // Sliders [H, S, B]
  sliders = [createSlider(0, 360, 360, 1), createSlider(0, 100, 100, 1), createSlider(0, 100, 100, 1)];

  img = createImage(pd * width, pd * height);

  frameRate(5);

  tooltips = [createP('rgb(0,0,0)'), createP('hsb(0,0,0)')];

  colorMode(HSB, 360, 100, 100, 255);
}

function mousePressed() {
  let clr = xy2color(mouseX - img.width / 2, mouseY - img.height / 2);
  tooltips[0].html('rgb(' + floor(red(clr)) + ',' + floor(green(clr)) + ',' + floor(blue(clr)) + ')');
  tooltips[1].html('hsb(' + floor(hue(clr)) + ',' + floor(saturation(clr)) + ',' + floor(brightness(clr)) + ')');
}

function hsb() {
  return createVector(h(), s(), b());
}

function h() {
  return sliders[0].value();
}

function s() {
  return sliders[1].value();
}

function b() {
  return sliders[2].value();
}

function xy2color(x, y) {
  // Convert Cartesian coordinates into HSB/polar coordinates
  let h_ = floor((atan2(y, x) + PI) * 180 / PI);
  let s_ = floor(sqrt(x * x + y * y) / rmax * 100);

  // Set black if outside the HSB circle
  if (h_ > h() || s_ > s()) {
    return color(0, 0, 0, 0);
  } else {
    return color(h_, s_, b(), 255);
  }
}

function col2xy(clr) {
  let ang = hue(clr) * 180 / PI;
  let rad = saturation(clr) / 100 * rmax;

  return [rad * cos(ang), rad * sin(ang)];
}

function draw() {
  let clr;
  let mag;
  let idx;
  let x_, y_;
  let rd, grn, bl, lph;

  background(0);

  img.loadPixels();

  // Loop over all of img's pixels
  for (let py = 0; py < img.height; py++) {
    for (let px = 0; px < img.width; px++) {
      // Get color for the current coordinate
      clr = xy2color(px - img.width / 2, py - img.height / 2);

      rd = red(clr);
      grn = green(clr);
      bl = blue(clr);
      lph = alpha(clr);

      for (let i = 0; i < pd; i++) {
        for (let j = 0; j < pd; j++) {
          // loop over
          idx = 4 * ((py * pd + j) * img.width * pd + (px * pd + i));
          img.pixels[idx + 0] = rd;
          img.pixels[idx + 1] = grn;
          img.pixels[idx + 2] = bl;
          img.pixels[idx + 3] = lph;
        }
      }
    }
  }
  img.updatePixels();
  image(img, 0, 0);
}
