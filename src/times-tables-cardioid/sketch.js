let radius = 0;
let totalSlider;
let root;

function setup() {
  createCanvas(400, 400);

  radius = 0.48 * width;
  
  root = createVector(-radius, 0);

  totalSlider = createSlider(2, 500, 500, 1);
  factorSlider = createSlider(-10, 10, 2, 0.1);
  
  colorMode(HSB)
}

function factor() {
  return factorSlider.value();
}

function total() {
  return totalSlider.value();
}

function getPoint(i) {
  return p5.Vector.fromAngle(map(sign(i) * (i % total()), 0, total(), PI, PI + TWO_PI)).mult(radius);
}

function getPair(i) {
  return [getPoint(i), getPoint(i * factor())];
}

function draw() {
  background(31);
  translate(width / 2, height / 2);
  
  strokeWeight(0.5);
  stroke(255);
  noFill();
  circle(0, 0, 2 * radius);
  
  strokeWeight(8);
  point(root.x, root.y);

  strokeWeight(2);
  
  let cstart = color(0, 100, 100);
  let cend = color(359, 100, 100);

  for (let i = 0; i < total(); i++) {
    let [n, n2] = getPair(i);
    stroke(lerpColor(cstart, cend, i / total()));
    line(n.x, n.y, n2.x, n2.y);
  }
}