class Branch {

  constructor(pos, dir, depth) {
    this.position = pos;
    this.direction = dir;
    this.depth = depth ? depth : 0;
    this.branches = [];
  }
  
  get x() {
    return this.position.x;
  }
  
  get y() {
    return this.position.y;
  }
  
  get u() {
    return this.direction.x;
  }
  
  get v() {
    return this.direction.y;
  }

  get heading() {
    return this.direction.heading();
  }

  get end() {
    return p5.Vector.add(this.position, this.direction);
  }

  get len() {
    return this.direction.mag();
  }

  draw() {
    stroke(255);
    strokeWeight(8 * exp(-this.depth / 4));
    line(this.x, this.y, this.end.x, this.end.y);

    // Draw all child branches
    for (let branch of this.branches) {
      branch.draw();
    }
  }

  grow() {
    if (this.branches.length) {
      for (let branch of this.branches) {
        branch.grow();
      }
    } else {
      // No branches yet, so let's add new branches
      for (let i = 0; i < params.branching; i++) {
        this.branches.push(new Branch(this.end, this.direction.copy().rotate(params.rotation * (i - ( params.branching - 1 ) / 2)).mult(params.reduction), this.depth + 1));
      }
    }
  }

}
