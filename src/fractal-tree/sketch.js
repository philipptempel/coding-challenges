let params = {
  branching: 2,
  branchingMin: 1,
  branchingMax: 10,
  branchingStep: 1,
  //
  len: 100,
  lenMin: 10,
  lenMax: 500,
  lenStep: 10,
  //
  rotation: 1 / 6,
  rotationMin: 0,
  rotationMax: 1 / 5,
  rotationStep: 0.01,
  //
  reduction: 0.85,
  reductionMin: 0.10,
  reductionMax: 0.99,
  reductionStep: 0.01,
}

let gui;

let tree;

function setup() {
  createCanvas(800, 800);

  params.rotation = PI / 6;
  params.rotationStep = PI / 12;
  params.rotationMax = PI;

  makeGui();

  let root = createVector(width / 2, height);
  let dir = createVector(0, -params.len);

  tree = new Branch(
    createVector(width / 2, height),
    createVector(0, -params.len));
}

function makeGui() {
  gui = createGui("Tree Control");
  gui.addObject(params);
  gui.setPosition(5, height + 5);
}

function mousePressed() {
  if (0 < mouseX && mouseX < width && 0 < mouseY && mouseY < height) {
    tree.grow();
  }
}

function draw() {
  background(51);
  tree.draw();
}
