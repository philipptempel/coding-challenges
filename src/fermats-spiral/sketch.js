let phi = 0;
let lenp = 0;
let lenn = 0;

let gui;
let params = {
  a: 10,
  aMin: 1,
  aMax: 20,
  aStep: 0.1,
  delta: 1.00,
  deltaMin: 0.01,
  deltaMax: 2.00,
  deltaStep: 0.01,
  perFrame: 100,
  perFrameMin: 10,
  perFrameMax: 1000,
  perFrameStep: 10,
}
let previousParams = {
}

function setup() {
  createCanvas(400, 400);
  background(31);
  colorMode(HSB);

  max_r = sqrt(width * width + height * height) / 2;

  setupGui();
}

function setupGui() {
  gui = createGui("Fermat's Spiral");
  gui.addObject(params);
  gui.setPosition(5, height + 5);
  QuickSettings.setGlobalChangeHandler(guiChange);
}

function guiChange(t) {
  if ( t === "a" || t === "delta" ) {
    clear();
    phi = lenp = lenn = 0;
    colorMode(RGB);
    background(31);
    colorMode(HSB);
  }
}

function draw() {
  translate(width / 2, height / 2);

  var cp, rp, hup;
  var cn, rn, hun;
  let pr;

  let a = params.a;

  for (let i = 0; i < params.perFrame; i++) {
    pr = phi * PI / 180;
    rp = +a * sqrt(pr);
    rn = -a * sqrt(pr);

    hup = (pow(lenp, 2 / 3)) % 256;
    hun = (pow(lenn, 2 / 3)) % 256;

    cp = p5.Vector.fromAngle(-pr).setMag(rp);
    cn = p5.Vector.fromAngle(-pr).setMag(rn);

    stroke(hup, 255, 255);
    point(cp.x, cp.y);

    stroke(hun, 255, 255);
    point(cn.x, cn.y);

    var delta_ = params.delta / (1 + pow(phi, 1 / 4));

    phi += delta_;
    lenp += 2 * rp * delta_;
    lenn += -2 * rn * delta_;
  }

  if (cp.mag() > max_r) {
    noLoop();
  }

}
