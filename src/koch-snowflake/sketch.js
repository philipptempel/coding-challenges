let vertices = [];
let edges = [];
let kiter = 0;
let radius;

function setup() {
  createCanvas(800, 800);
  
  radius = height / 2;

  let ang;
  for (let i = 0; i < 3; i++) {
    ang = i * TWO_PI / 3 - HALF_PI;
    vertices.push(p5.Vector.fromAngle(ang).setMag(radius));
    edges.push([i, (i + 1) % 3]);
  }

  frameRate(1);
}

function edgeLength() {
  return p5.Vector.sub(vertices[0], vertices[1]).mag();
}

function iterate() {
  let newvertices = [];
  let newedges = [];

  let e;
  let midpoint;
  let cntr = 0;
  let rotates = [0, 1, -2, 1];
  let edgelength = edgeLength();
  let newedgelength = edgelength / 3;
  
  let current, next, dirn;

  // Only calculate new vertices if edge length is larger than 2 pixels
  if (newedgelength >= 1) {
    let nnewvertices = vertices.length * 4;

    for (let edge of edges) {
      // Step direction
      dirn = p5.Vector.sub(vertices[edge[1]], vertices[edge[0]]).normalize().setMag(newedgelength);
      
      // Current vertex
      current = vertices[edge[0]];

      // Loop over all rotates
      for (let f of rotates) {
        // New vertex
        next = p5.Vector.add(current, dirn.rotate(-f * 60 * PI / 180));
        newvertices.push(next);
        newedges.push([cntr, ++cntr % nnewvertices]);
        current = next;
      }
    }

    vertices = newvertices;
    edges = newedges;
  }
}

function draw() {
  background(220);

  translate(width / 2, height / 2);
  fill(31, 75);
  beginShape();
  for (let edge of edges) {
    vertex(vertices[edge[0]].x, vertices[edge[0]].y);
  }
  endShape(CLOSE);
  iterate();
}
