Array.prototype.contains = Array.prototype.contains || function(obj) {
  var i, l = this.length;
  for (i = l; i >= 0; i--) {
    if (this[i] === obj) {
      return true;
    }
  }
  return false;
};

function rows(sz) {
  return sz[0];
}

function cols(sz) {
  return sz[1];
}

function sub2ind(sz, i, j) {
  return j + i * cols(sz);
}

function ind2sub(sz, idx) {
  let i = floor(idx / rows(sz));
  let j = idx % rows(sz);

  return [i, j];
}

function sign(a) {
  return a / abs(a);
}
