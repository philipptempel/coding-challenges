let pd;
// Cell side length
let cellside;
// Shape of the window ROWS X COLS
let shape = [];
// Generated maze
let maze = [];
// Current cell used in generation algorithm
let current;
// Stack of cells in generation process
var stack = [];

var total = 0;
var visited = 0;

function setup() {
  createCanvas(1200, 1000);

  pd = 2;
  pixelDensity(pd);

  cellside = 40;

  // [ rows , cols ]
  shape = [height / cellside, width / cellside];

  let x = cellside / 2;
  let y = cellside / 2;

  // Build maze in column major i.e., fill column row, then fill second column, etc.
  for (let c = 0; c < cols(); c++) {
    for (let r = 0; r < rows(); r++) {
      maze.push(new Cell((c + 1 / 2) * cellside, (r + 1 / 2) * cellside, cellside));
    }
  }

  // Activate a random cell to be the start of the maze algorithm
  current = random(maze);
  total = maze.length;
  visited++;

}

function rows() {
  return shape[0];
}

function cols() {
  return shape[1];
}

function sub2ind(sz, r, c) {
  if (r < 0 || r >= sz[0] || c < 0 || c >= sz[1]) {
    return undefined;
  }

  return r + c * sz[0];
}

function xy2ind(x, y) {
  return sub2ind(shape, floor(y / cellside), floor(x / cellside));
}

function xy2cell(x, y) {
  return maze[xy2ind(x, y)];
}

function cell2ind(c) {
  return xy2ind(c.x, c.y);
}

function draw() {
  background(61);
  drawCells();

  current.visit();

  // STEP 1
  var next = random(current.neighbors());
  if (next) {
    // STEP 2: add current cell to the stack of visited cells
    stack.push(current);

    // STEP 3: connect current cell with next cell
    current.connect(next);

    // Mark next cell as visited
    next.visit();
    visited++;

    // STEP 4: Proceed to next cell
    current = next;
  } else if (visited == total) {
    console.log('Done');
    noLoop();
  } else if (stack.length > 0) {
    current = stack.pop();
  }
}

function drawCells() {
  for (let cell of maze) {
    cell.draw();
  }
}
