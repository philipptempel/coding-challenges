class Cell {
  constructor(x, y, sl) {
    this.x = x;
    this.y = y;
    this.side = sl || 5;
    // right, top, left, bottom
    this.walls = [true, true, true, true];
    this.visited = false;
    this.highlighted = false;
  }

  neighbors() {
    let wc = this.walls.length;
    let dangle = TWO_PI / wc;
    let x = this.x;
    let y = this.y;
    let idx;

    let neighbors_ = [];
    let c;

    for (let w = 0; w < wc; w++) {
      x = floor(this.x + this.side * cos(w * dangle));
      y = floor(this.y + this.side * sin(w * dangle));

      c = xy2cell(x, y);

      if (c && ! c.visited) {
        neighbors_.push(c);
      }
    }

    return neighbors_;
  }

  left() {
    return xy2cell(this.x - this.side, this.y);
  }

  right() {
    return xy2cell(this.x + this.side, this.y);
  }

  above() {
    return xy2cell(this.x, this.y - cellside);
  }

  below() {
    return xy2cell(this.x, this.y + cellside);
  }

  leftOf(other) {
    return this.right() === other;
  }

  rightOf(other) {
    return this.left() === other;
  }

  aboveOf(other) {
    return this.below() === other;
  }

  belowOf(other) {
    return this.above() === other;
  }

  visit() {
    this.visited = true;
  }

  highlight() {
    this.highlighted = true;
  }
  
  unhighlight() {
    this.highlighted = false;
  }

  connect(other) {
    if (this.rightOf(other)) {
      this.walls[2] = false;
      other.walls[0] = false;
    } else if (this.leftOf(other)) {
      this.walls[0] = false;
      other.walls[2] = false;
    } else if (this.belowOf(other)) {
      this.walls[1] = false;
      other.walls[3] = false;
    } else if (this.aboveOf(other)) {
      this.walls[3] = false;
      other.walls[1] = false;
    }
  }

  draw() {
    rectMode(CENTER);
    push();
    noFill();
    strokeWeight(0);
    translate(this.x, this.y);

    if (this.visited) {
      push();
      fill(255);
      square(0, 0, this.side);
      pop();
    }
    
    if (this.highlighted) {
      push();
      fill(0, 0, 255);
      noStroke();
      square(0, 0, 0.9 * this.side);
      pop();
    }

    let wc = this.walls.length;
    let dangle = TWO_PI / wc;
    for (let w = 0; w < wc; w++) {
      push();

      if (this.walls[w]) {
        rotate(-w * dangle);
        stroke(0);
        strokeWeight(4);
        line(this.side / 2, -this.side / 2, this.side / 2, this.side / 2);
      }

      pop();
    }

    pop();
  }

}
