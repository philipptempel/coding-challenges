let pi;
let cols;
let rows;

let colorpalette = [];

let mark = false;

function preload() {
  pi = loadStrings('pi-1million.txt');
}

function setup() {
  createCanvas(600, 1000);
  pi = pi[0];
  
  colorpalette = ["#fff100", "#ff8c00", "#e81123", "#ec008c", "#68217a", "#00188f", "#00bcf2", "#00b294", "#009e49", "#bad80a"];

  cols = 40;
  rows = 60;
}

function keyPressed() {
  if (keyCode == ENTER ) {
    mark = !mark;
    redraw();
  }
}

function draw() {
  background(220);

  let w = width / cols;
  let h = height / rows;
  let idx = 0;
  let n = "";
  let ts = 0.90 * min(w, h);
  
  noFill();
  colorMode(RGB);
  for (y = 0; y < height; y += h) {
    for (x = 0; x < width; x += w) {
      n = float(pi[idx]);

      strokeWeight(0.25);
      textAlign(CENTER, CENTER);
      textSize(ts);
      fill(colorpalette[n % colorpalette.length]);
      rect(x, y, w, h);
      
      if (mark) {
        strokeWeight(0)
        fill(colorpalette[floor((n + colorpalette.length/2) % colorpalette.length)]);
        text(n, x + w / 2, y + h / 2);
      }

      idx++;
    }
  }

  noLoop();
}