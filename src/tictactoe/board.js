class Board {
  constructor(cells, players) {
    this.cells = cells;
    this.available = cells.map((_, i) => i);
    this.players = players;
    this.winner = undefined;
    // A random initial player
    this.current = floor(random(this.players.length));
  }

  get player() {
    return this.players[this.current];
  }

  next() {
    this.current++;
    if (this.current >= this.players.length) {
      this.current = 0;
    }
  }

  get cell() {
    return this.cells[this.available.splice(floor(random(this.available.length)), 1)]
  }

  finished() {
    return this.winner ? 2 : this.available.length == 0;
  }

  edges() {
    let winner = this.winner;
    let win_count = 3;

    if (!winner) {
      // Check all rows
      for (let r = 0; r < rows; r++) {
        if (winner) {
          break;
        }

        let cnt = 0;
        let prev = -1;

        for (let c = 0; c < cols; c++) {
          let player = this.cells[sub2ind(r, c)].player;

          if (prev === player || prev === -1) {
            cnt++;
          } else {
            cnt = 0;
          }

          if (cnt >= win_count) {
            winner = player;
            break
          }

          prev = player;
        }
      }

      // Check all columns (if no row has won)
      if (!winner) {
        for (let c = 0; c < cols; c++) {
          if (winner) {
            break;
          }

          let cnt = 0;
          let prev = -1;

          for (let r = 0; r < rows; r++) {
            let player = this.cells[sub2ind(r, c)].player;

            if (prev === player || prev === -1) {
              cnt++;
            } else {
              cnt = 0;
            }

            if (cnt >= win_count) {
              winner = player;
              break;
            }

            prev = player;
          }
        }
      }

      // Check right-down diagonals
      if (!winner) {
        for (let c = 0; c < cols; c++) {
          if (winner) {
            break;
          }

          let cnt = 0;
          let prev = -1;

          for (let r = 0; r < rows; r++) {
            let c1 = this.cells[sub2ind(r, c)];
            let c2 = this.cells[sub2ind(r + 1, c + 1)];
            let c3 = this.cells[sub2ind(r + 2, c + 2)];
            if (c1 
                && c2 
                && c3 
                && c1.player
                && c2.player
                && c3.player
                && c1.player === c2.player
                && c2.player === c3.player) {
              winner = c1.player;
              break;
            }
          }
        }
      }

      // Check right-up diagonals
      if (!winner) {
        for (let r = 0; r < rows; r++) {
          if (winner) {
            break;
          }

          let cnt = 0;
          let prev = -1;

          for (let c = 0; c < cols; c++) {
            let c1 = this.cells[sub2ind(r, c)];
            let c2 = this.cells[sub2ind(r + 1, c - 1)];
            let c3 = this.cells[sub2ind(r + 2, c - 2)];
            if (c1
                && c2
                && c3
                && c1.player
                && c2.player
                && c3.player
                && c1.player === c2.player
                && c2.player === c3.player) {
              winner = c1.player;
              break;
            }
          }
        }
      }

      this.winner = winner;

    }

  }

  update() {
    // Get a random free cell
    let cell = this.cell;

    // If there was a free cell
    if (cell) {
      // Mark the cell given the current player
      cell.mark(this.player);

      // Check if there are any rows or columns of three
      this.edges();

      // Next player
      this.next();
    }
  }

  draw() {
    for (let cell of this.cells) {
      cell.draw();
    }
  }

}
