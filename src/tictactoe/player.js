class Player {

  constructor(marker) {
    this.marker = marker;
    this.cells = [];
  }
  
  draw() {
    fill(0);
    textAlign(CENTER, CENTER);
    text(this.marker, 0, 0);
  }

}
