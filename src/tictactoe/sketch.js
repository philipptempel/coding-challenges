let rows;
let cols;

let tictactoe;

let pWinner;

function setup() {
  createCanvas(400, 400);

  rows = 3;
  cols = 3;

  dims = createVector(width / cols, height / rows);
  
  createButton("erase").mousePressed(factory);
  pWinner = createP("");
  
  factory();
  
  frameRate(5);
}

function factory() {
  let cells = [];
  let players = [];

  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      cells.push(new Cell(createVector(i, j), dims));
    }
  }
  
  players.push(new Player('x'));
  players.push(new Player('o'));
  
  tictactoe = new Board(cells, players);
  pWinner.html("");
  
  loop();
}

function draw() {
  background(220);
  
  tictactoe.draw();
  
  let finished = tictactoe.finished();
  
  if ( finished ) {
    if ( finished == 2 ) {
      pWinner.html("Winner:&nbsp;" + tictactoe.winner.marker);
    } else {
      pWinner.html("Tie");
    }
    noLoop();
  }
  
  tictactoe.update();
  
}

function sub2ind(i, j) {
  return 0 <= i && 0 <= j && j <= cols ? j + i * cols : undefined;
}

function ind2sub(idx) {
  return {'i': idx % cols, 'j': floor(idx / cols)};
}
