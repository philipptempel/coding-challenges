class Cell {

  constructor(idx, dims) {
    this.index = idx;
    this.dimensions = dims;
    this.player = undefined;
  }

  get x() {
    return (this.index.y + 0.5) * this.dimensions.x;
  }

  get y() {
    return (this.index.x + 0.5) * this.dimensions.y;
  }
  
  get ij() {
    return this.index.array();
  }

  get width() {
    return this.dimensions.x;
  }

  get height() {
    return this.dimensions.y;
  }

  mark(p) {
    this.player = p;
  }

  draw() {
    push();
    translate(this.x, this.y);

    fill(255);
    stroke(0);
    strokeWeight(2);

    rectMode(CENTER);
    rect(0, 0, this.width, this.height);
    
    if (this.player !== undefined ) {
      textSize(0.75 * min(this.width, this.height));
      this.player.draw();
    }

    pop();
  }

}
