let d;
let zoom;
let scl;

function setup() {
  createCanvas(400, 400);
  pixelDensity(1);

  d = 5;
  zoom = 1;
  scl = 5;
}

function zoomLevel() {
  return pow(1 / zoom, 2/3);
}

function draw() {
  var maxiter = 100;

  loadPixels();

  for (var c = 0; c < width; c++) {
    for (var r = 0; r < height; r++) {
      var x = zoomLevel() * scl * (c / width - 1 / 2);
      var y = zoomLevel() * scl * (r / height - 1 / 2);

      var idx = c + r * width * 4;

      var kiter = 0;

      var re = x;
      var im = y;

      while (kiter < maxiter) {
        var rere = re * re - im * im;
        var imim = 2 * re * im;
        re += rere;
        im += imim;
        if (re * re + im * im > 16) {
          break;
        }
        kiter++;
      }

      var brightness = 0;
      if (kiter < maxiter) {
        brightness = map(kiter, 0, maxiter, 0, 1);
        brightness = map(sqrt(brightness), 0, 1, 0, 255);
      }

      pixels[idx + 0] = brightness;
      pixels[idx + 1] = brightness;
      pixels[idx + 2] = brightness;
      pixels[idx + 3] = 255;
      
    }
  }

  updatePixels();
}
