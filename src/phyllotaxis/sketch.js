let n = 0;

let gui;
let params = {
  angle: 137.5,
  angleMin: 10,
  angleMax: 180,
  angleStep: 0.5,
  c: 4,
  cMin: 1,
  cMax: 10,
  cStep: 1,
  radius: 4,
  radiusMin: 1,
  radiusMax: 10,
  radiusStep: 0.5
}

function setup() {
  createCanvas(800, 800);
  background(11);

  angleMode(DEGREES);
  colorMode(HSB);

  setupGui();
}

function setupGui() {
  gui = createGui("Phyllotaxis");
  gui.setPosition(5, 5);
  gui.addObject(params);
  QuickSettings.setGlobalChangeHandler(updateGui);
}

function updateGui(t) {
  clear();
  colorMode(RGB);
  background(11);
  colorMode(HSB);
  n = 0;
}

function draw() {
  let c = params.c;
  let angle = params.angle;
  let radius = params.radius;

  var phi = n * angle;
  var r = c * sqrt(n);

  let p = p5.Vector.fromAngle(phi * PI / 180).setMag(r);

  // Determine HUE depending on some formula I came up with
  var hu = map(cos(c * phi) * sin(n + c * phi), 0, 1, 0, phi);

  // Translate to center of canvas
  translate(width / 2, height / 2);
  // Set fill color of circle
  fill(hu % 256, (hu * hu) % 256, 255);
  noStroke();
  // And draw circle
  circle(p.x, p.y, radius);

  n++;

}
