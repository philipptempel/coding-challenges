class Flake {

  constructor() {
    this.position = createVector(random(width));
    this.velocity = createVector();
    this.acceleration = createVector();
    this.shape = undefined;
    this.dimension = undefined;
    this.maxVelocity = undefined;
    
    this.reset();
    
    this.position.y = random(-height, height);
  }
  
  applyForce(force) {
    this.acceleration.add(force);
  }
  
  edges() {
    if ( this.position.x < 0 ) {
      this.position.x = width;
    } else if ( this.position.x > width ) {
      this.position.x = 0;
    }
    
    if ( this.position.y > height ) {
      this.reset();
    }
  }
  
  reset() {
    this.shape = random(['circle', 'square', 'triangle']);
    this.position.y = -random(0, height);
    this.position.z = this.dimension = random(2, 8);
    this.maxVelocity = 6 / 13 * this.dimension;
  }

  update() {
    this.position.add(this.velocity);
    this.velocity.add(this.acceleration).limit(this.maxVelocity);

    this.acceleration.mult(0);
  }

  draw() {
    push();
    
    noStroke();
    translate(this.position.x, this.position.y);

    if (this.shape === 'circle') {
      circle(0, 0, 2 * this.dimension);
    } else if (this.shape === 'square') {
      square(0, 0, 2 * this.dimension);
    } else if ( this.shape === 'triangle') {
      beginShape(TRIANGLES);
      let ang;
      for ( let i = 0; i < 3; i++ ) {
        ang = i * TWO_PI / 3 + HALF_PI;
        vertex(this.dimension * cos(ang), this.dimension * sin(ang))
      }
      endShape();
    }

    pop();
  }

}
