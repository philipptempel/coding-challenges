let snow = [];

let nflakes = 200;
let windmag = 2;
let windoff = 0;
let gravity;

function setup() {
  createCanvas(windowWidth, windowHeight);

  gravity = createVector(0, 2, 0);

  let flake;
  for (let i = 0; i < nflakes; i++) {
    flake = new Flake();
    snow.push(flake);
  }
}

function draw() {
  background(31);

  let wind;

  for (let flake of snow) {
    wind = map(
        noise(
            map(flake.position.x, 0, width, -1, 1),
            map(flake.position.y, 0, height, -1, 1),
            flake.position.z + windoff
        ),
        0,
        1,
        -windmag / 2,
        windmag / 2
    );

    flake.draw();

    flake.applyForce(gravity);
    flake.applyForce(wind);

    flake.update();

    flake.edges();
  }

  windoff += random(0.01, 0.05);
}
