var axiom;
var angle;
var sentence;
var len;
var rules = [];
var button;

function setup() {
  createCanvas(400, 400);

  angle = radians(25);
  axiom = "F";
  rules = [];
  rules.push({
    a: "F",
    b: "FF+[+F-F-F]-[-F+F+F]"
  })

  createButton("iterate").mousePressed(iterate);
  createButton("reset").mousePressed(reset);

  reset();

  frameRate(5);
}

function mousePressed(e) {
  if (0 <= mouseX && mouseX <= width && 0 <= mouseY && mouseY <= height) {
    if (mouseButton == LEFT) {
      iterate();
    }
  }
}

function doubleClicked() {
  if (0 <= mouseX && mouseX <= width && 0 <= mouseY && mouseY <= height) {
    reset();
  }
}

function draw() {
  background(61);

  resetMatrix();
  translate(width / 2, height);

  strokeWeight(2);
  stroke(255, 100);

  var current;

  for (let c of sentence) {
    if (c == "F") {
      line(0, 0, 0, -len);
      translate(0, -len);
    } else if (c == "[") {
      push();
    } else if (c == "]") {
      pop();
    } else if (c == "+") {
      rotate(angle);
    } else if (c == "-") {
      rotate(-angle);
    }
  }
}

function iterate() {

  var newsentence = "";
  var found = false;

  len *= 0.5;

  for (let c of sentence) {
    found = false;
    for (let rule of rules) {
      if (c == rule.a) {
        found = true;
        newsentence += rule.b;
        break;
      }
    }
    if (!found) {
      newsentence += c;
    }
  }

  sentence = newsentence;

  createP(sentence);
}

function reset() {
  sentence = axiom;
  len = 100;

  createP(sentence);
}
