let stars = [];
let speed = 4;

function setup() {
  createCanvas(windowWidth, windowHeight);

  for (let i = 0; i < 100; i++) {
    stars.push(
      new Star(
        random(-width, width) / 2,
        random(-height, height) / 2,
        random(width)
      )
    );
  }
}

function draw() {
  background(11);
  translate(width / 2, height / 2);

  speed = exp(map(constrain(mouseX, 0, width), 0, width, 0, log(101))) - 1;

  for (let star of stars) {
    star.draw();
    star.update();
  }
}
