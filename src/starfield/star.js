class Star {
  constructor(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.pz = z;
  }

  update() {
    this.pz = this.z;
    this.z -= speed;

    if (this.z < 0) {
      this.reset();
    }
  }

  reset() {
    this.x = random(-width, width) / 2;
    this.y = random(-height, height) / 2;
    this.z = width;
    this.pz = this.z;
  }

  draw() {
    push();

    var sx = map(this.x / this.z, 0, 1, 0, width);
    var sy = map(this.y / this.z, 0, 1, 0, height);
    var r = map(this.z, 0, width, 16, 0);
    var px = map(this.x / this.pz, 0, 1, 0, width);
    var py = map(this.y / this.pz, 0, 1, 0, height);

    // Star
    stroke(255);
    strokeWeight(r);
    point(sx, sy);

    // Trail
    stroke(255);
    strokeWeight(r);
    line(px, py, sx, sy);
  }
}
