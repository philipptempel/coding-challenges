let cam;
let vd = 12;

function setup() {
  createCanvas(640, 480);

  cam = createCapture(VIDEO);

  cam.size(width / vd, height / vd);
  cam.hide();

}

function sub2ind(sz, i, j) {
  return j + i * sz[1];
}

function draw() {
  background(51);

  cam.loadPixels();
  let idx;
  let sz = [cam.height, cam.width];
  let r, g, b, br, w;

  for (var y = 0; y < cam.height; y++) {
    for (var x = 0; x < cam.width; x++) {
      idx = sub2ind(sz, y, sz[1] - x + 1) * 4;
      r = cam.pixels[idx + 0];
      g = cam.pixels[idx + 1];
      b = cam.pixels[idx + 2];
      h = cam.pixels[idx + 3];

      br = (r + g + b) / 3;
      w = br / 255 * vd;

      noStroke();
      fill(255);
      circle(x * vd, y * vd, w);
    }
  }

}
