class Box {

  constructor(pos, dims, col) {
    this.position = pos;
    this.dims = dims;
    this.color = col;
  }

  get w() {
    return this.dims.x;
  }

  get d() {
    return this.dims.z;
  }

  get h() {
    return this.dims.y;
  }

  get x() {
    return this.position.x;
  }

  get y() {
    return this.position.y;
  }

  get z() {
    return this.position.z;
  }
  
  get r() {
    return red(this.color);
  }
  
  get g() {
    return green(this.color);
  }
  
  get b() {
    return blue(this.color);
  }

  draw() {
    push();
    strokeWeight(0);
    translate(this.x, this.y, this.z);
    fill(this.color);
    box(this.w, this.h, this.d);
    pop();
  }

}