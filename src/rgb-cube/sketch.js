let nr;
let ng;
let nb;
let rgbsliders = [];
let wdhsliders = [];
let cntsliders = [];

let boxes = [];

function setup() {
  createCanvas(400, 400, WEBGL);

  nr = 10;
  ng = 10;
  nb = 10;

  // Box counts
  createP('Box counts [ r / g / b]').style('color: #FFFFFF');
  cntsliders = [createSlider(2, 15, 10, 1), createSlider(2, 15, 10, 1), createSlider(2, 15, 10, 1)];
}

function rcount() {
  return cntsliders[0].value();
}

function gcount() {
  return cntsliders[1].value();
}

function bcount() {
  return cntsliders[2].value();
}

function boxwidth() {
  return 20;
}

function boxheight() {
  return 20;
}

function boxdepth() {
  return 20;
}

function rspacing(r) {
  return boxwidth();
}

function gspacing(g) {
  return boxheight();
}

function bspacing(b) {
  return boxdepth();
}

function mousePressed() {
  console.log(mouseX, mouseY);
}

function makeCubes() {
  let idx;

  // Width of all boxes
  let wr = boxwidth();
  let wg = boxheight();
  let wb = boxdepth();
  
  // Spacing of boxes
  let sr = rspacing();
  let sg = gspacing();
  let sb = bspacing();
  
  // Number of boxes in each dimension
  nr = rcount();
  ng = gcount();
  nb = bcount();
  
  // Empty array
  boxes = [];
  
  let pos;
  let dims;
  let col;

  // Loop over each cube
  for (let ir = 0; ir < nr; ir++) {
    for (let ig = 0; ig < ng; ig++) {
      for (let ib = 0; ib < nb; ib++) {
         boxes.push(new Box(createVector(ir * sr, ig * sg, ib * sb), createVector(wr, wg, wb), color(ir / nr * 255, ig / ng * 255, ib / nb * 255)));
      }
    }
  }
}

function draw() {
  background(220);
  
  makeCubes();

  orbitControl();

  // More visible R/G/B axes
  push();
  strokeWeight(4);
  translate(-boxwidth(), -boxheight(), -boxdepth());
  // R axis
  stroke(255, 0, 0);
  line(0, 0, 0, rcount() * boxwidth(), 0, 0);
  // G axis
  stroke(0, 255, 0);
  line(0, 0, 0, 0, gcount() * boxheight(), 0);
  // B axis
  stroke(0, 0, 255);
  line(0, 0, 0, 0, 0, bcount() * boxdepth());
  pop();

  for ( let box of boxes) {
    box.draw();
  }

  strokeWeight(1);
  stroke(0);
}
