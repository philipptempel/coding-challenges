class Food {
  constructor(pos) {
    this.position = createVector(...pos);
  }
  
  get x() {
    return this.position.x;
  }
  
  get y() {
    return this.position.y;
  }
  
  draw() {
    push();
    noStroke();
    fill(255, 0, 0);
    rect(this.x, this.y, 1, 1);
    pop();
  }
  
}