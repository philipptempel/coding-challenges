let size = [20, 20];

let snake;
let food;

function setup() {
  createCanvas(400, 400);
  frameRate(5);

  snake = new Snake([size[1] / 2, size[0] / 2], randomDirection());
  food = new Food(randomXY());
}

function randomXY() {
  return [floor(random(size[1])), floor(random(size[0]))]
}

function keyPressed() {
  snake.move(keyCode);
  if ( key == " " ) {
    snake.grow();
  }
}

function randomDirection() {
  return random([LEFT_ARROW, RIGHT_ARROW, UP_ARROW, DOWN_ARROW]);
}

function mousePressed() {
  if (snake.dead()) {
    snake = new Snake(randomXY());
    food = new Food(randomXY());
    loop();
  }
}

function newFood() {
  return createVector(floor(random(width / size[1])), floor(random(height / size[0])));
}

function gameover() {
  noLoop();
  
  background(0, 128);

  push();

  translate(size[1] / 2, size[0] / 2);
  textAlign(CENTER, CENTER);
  fill(0);
  stroke(255);
  strokeWeight(0.10);
  textSize(0.50 * min(size[1] / 2, size[0] / 2));
  text("GAME\nOVER", 0, 0);

  pop();
}

function draw() {
  scale(size[0], size[1]);
  background(220);

  // Draw everything
  snake.draw();
  food.draw();

  // Move snake
  snake.update();

  // Check if snake dead now
  if (snake.dead()) {
    gameover();
  }

  // Check if snake ate the food
  if (snake.eats(food)) {
    food = new Food(randomXY());
  }
}