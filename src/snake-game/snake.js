class Snake {
  constructor(pos, move) {
    this.body = [createVector(...pos)];
    this._moves = undefined;
    this._direction = createVector(0, 0);
    this.move(move);
  }

  get head() {
    return this.body[this.length - 1];
  }

  get tail() {
    return this.body[0];
  }

  get x() {
    return this.head.x;
  }

  get y() {
    return this.head.y;
  }

  get direction() {
    return this._direction;
  }

  get length() {
    return this.body.length;
  }

  set direction(d) {
    this._direction = createVector(...d);
  }
  
  move(h) {
    switch (h) {
      case RIGHT_ARROW:
        this.right();
        break;
        
      case UP_ARROW:
        this.up();
        break;
        
      case LEFT_ARROW:
        this.left();
        break;
        
      case DOWN_ARROW:
        this.down();
        break;
        
      case undefined:
        this.halt();
        break;
        
      default:
        break;
    }
  }
  
  halt() {
    this._moves = undefined;
    this.direction = [0, 0];
  }

  up() {
    if ( this._moves !== DOWN_ARROW) {
      this._moves = UP_ARROW;
      this.direction = [0, -1];
    }
  }

  down() {
    if ( this._moves !== UP_ARROW) {
      this._moves = DOWN_ARROW;
      this.direction = [0, +1];
    }
  }

  left() {
    if ( this._moves !== RIGHT_ARROW) {
      this._moves = LEFT_ARROW;
      this.direction = [-1, 0];
    }
  }

  right() {
    if ( this._moves !== LEFT_ARROW) {
      this._moves = RIGHT_ARROW;
      this.direction = [+1, 0];
    }
  }

  eats(food) {
    let head = this.head;
    if ( food !== undefined ) {
      if (head.dist(food.position) < 1 ) {
        this.grow();
        return true;
      }
    }
    
    return false;
  }

  grow() {
    let head = this.head.copy();
    this.body.splice(0, 0, this.tail.copy());
  }

  dead() {
    return this.edges() || this.self();
  }

  self() {
    let head = this.head;
    for (let part of this.body) {
      if (part !== head && (part.x == head.x && part.y == head.y)) {
        return true;
      }
    }

    return false;
  }

  edges() {
    return this.head.x < 0 || size[1] <= this.head.x || this.head.y < 0 || size[0] <= this.head.y;
  }

  update() {
    this.body.push(this.head.copy().add(this.direction));
    this.body.splice(0, 1);
  }

  draw() {
    noStroke();
    stroke(0);
    strokeWeight(0.1);
    fill(255);
    for (let part of this.body) {
      rect(part.x, part.y, 1, 1);
    }
  }
}