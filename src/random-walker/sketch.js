let walkers = [];

const STEPSIZE = 5;
const NWALKERS = 5;

function setup() {
  createCanvas(400, 400);
  
  let clr = [[255, 0, 0], [0, 255, 0], [0, 0, 255]];
  
  for ( let i = 0; i < NWALKERS; i++) {
    walkers.push(new Walker(createVector(random(width), random(height)), [random(100, 255), random(100, 255), random(100, 255)]));
  }
  
  background(220);
}

function draw() {
  background(220);
  
  for ( let walker of walkers ) {
    walker.draw();
  }
  
  for ( let walker of walkers ) {
    walker.update();
  }
}