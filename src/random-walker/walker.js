const MOVE_UP = [0, 1];
const MOVE_DOWN = [0, -1];
const MOVE_LEFT = [-1, 0];
const MOVE_RIGHT = [1,0];
const MOVES = [MOVE_RIGHT, MOVE_UP, MOVE_LEFT, MOVE_DOWN];

class Walker {
  constructor(pos, clr) {
    this.position = pos || createVector(0, 0, 0);
    this.previous = this.position.copy();
    this.path = [];
    this.color = clr || [255, 0, 0];
  }

  get x() {
    return this.position.x;
  }

  get y() {
    return this.position.y;
  }
  
  get z() {
    return this.position.z;
  }
  
  set x(v) {
    this.position.x = v;
  }
  
  set y(v) {
    this.position.y = v;
  }
  
  set z(v) {
    this.position.z = v;
  }

  update() {
    this.position.add(createVector(...random(MOVES)).mult(STEPSIZE));
    this.path.push(this.position.copy());
    this.x = constrain(this.x, 0, width);
    this.y = constrain(this.y, 0, height);
  }

  draw() {
    push();
    stroke(...this.color, 150);
    strokeWeight(2);
    point(this.x, this.y);
    // Draw previous points
    for (let i = 1; i < this.path.length - 1; i++) {
      line(this.path[i-1].x, this.path[i-1].y, this.path[i].x, this.path[i].y);
    }
    pop();
  }
}