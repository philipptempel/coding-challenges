class Boid {

  constructor() {
    this.position = createVector(random(width), random(height));
    this.velocity = p5.Vector.random2D().setMag(random(2, 4));
    this.acceleration = createVector(0, 0);

    this.maxForce = 0.3; // random(0.8, 1.0);
    this.maxSpeed = 5.0; // random(3.0, 5.0);

    this.perceptionRadius = 50;
    this.separationRadius = 25;
    this.perceptionAngle = 90;
  }
  
  get x() {
    return this.position.x;
  }
  
  get y() {
    return this.position.y;
  }
  
  get vx() {
    return this.velocity.x;
  }
  
  get vy() {
    return this.velocity.y;
  }
  
  get ax() {
    return this.acceleration.x;
  }
  
  get ay() {
    return this.acceleration.y;
  }

  get heading() {
    return this.velocity.heading();
  }
  
  set x(x) {
    this.position.x = x;
  }
  
  set y(y) {
    this.position.y = y;
  }

  perceptible(boids) {
    let others = [];

    let d = Infinity;
    let r = this.perceptionRadius;

    let dir, head;

    for (let other of boids) {
      // Skip if boid is myself
      if (other === this) {
        continue;
      }

      d = dist(this.x, this.y, other.x, other.y);

      // Skip if outside of my perception radius
      if (r < d ) {
        continue;
      }

      // Direction from me to other boid
      dir = p5.Vector.sub(other.position, this.position);

      // Check if the other boid is within my range of perception
      let ang_other = atan2(dir.y, dir.x);
      head = this.heading;

      if (abs(head - ang_other) <= this.perceptionAngle) {
        others.push({
          'boid': other,
          'distance': d
        });
      }
    }

    return others;
  }

  edges() {
    if (this.x > width) {
      this.x = 0;
    } else if (this.x < 0) {
      this.x = width;
    }
    if (this.y > height) {
      this.y = 0;
    } else if (this.y < 0) {
      this.y = height;
    }
  }

  align(boids) {
    let steering = createVector(0, 0);
    let total = boids.length;

    for (let other of boids) {
      steering.add(other.boid.velocity);
      total++;
    }

    steering.div(total);
    steering.setMag(this.maxSpeed);
    steering.sub(this.velocity);
    steering.limit(this.maxForce);

    return steering;
  }

  separation(boids) {
    let steering = createVector();
    let total = boids.length;

    let d = Infinity;
    let diff = undefined;

    for (let other of boids) {
      // Skip boid if it is outside my separation radius
      if (other.distance > this.separationRadius) {
        total--;
        continue;
      }

      diff = p5.Vector.sub(this.position, other.boid.position);
      d = other.distance;

      diff.div(d * d);
      steering.add(diff);
    }

    if (total > 0) {
      steering.div(total);
      steering.setMag(this.maxSpeed);
      steering.sub(this.velocity);
      steering.limit(this.maxForce);
    }

    return steering;
  }

  cohesion(boids) {
    let steering = createVector();
    let total = boids.length;

    for (let other of boids) {
      steering.add(other.boid.position);
    }

    steering.div(total);
    steering.sub(this.position);
    steering.setMag(this.maxSpeed);
    steering.sub(this.velocity);
    steering.limit(this.maxForce);

    return steering;
  }

  cohort(boids) {
    let others = this.perceptible(boids);

    if ( others.length) {
      let alignment = this.align(others);
      let cohesion = this.cohesion(others);
      let separation = this.separation(others);

      alignment.mult(params.align);
      cohesion.mult(params.cohesion);
      separation.mult(params.separation);

      this.acceleration.add(alignment);
      this.acceleration.add(cohesion);
      this.acceleration.add(separation);
    }
  }

  update() {
    this.position.add(this.velocity);
    this.velocity.add(this.acceleration);
    this.velocity.limit(this.maxSpeed); 
    
    this.acceleration.mult(0);
  }

  show() {
    let heading = this.heading;
    let sr = this.separationRadius;
    let pr = this.perceptionRadius;
    let pa = radians(this.perceptionAngle);
    
    push();
    translate(this.x, this.y);

    noFill();
    rotate(heading);

    // Heading
    strokeWeight(2);
    stroke(255);
    beginShape(TRIANGLES);
    vertex(+10.0, 0.0);
    vertex(-5.0, -5.0);
    vertex(-5.0, +5.0);
    endShape();

    // Perception area
    strokeWeight(1);
    stroke(0, 255, 0);
    fill(255, 20);
    stroke(255, 150);
    arc(0, 0, 2 * pr, 2 * pr, -pa / 2, pa / 2);

    // Avoidance area
    noFill();
    strokeWeight(2);
    stroke(255, 0, 0, 100);
    circle(0, 0, 2 * sr);

    pop();
  }
}
