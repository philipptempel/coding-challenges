class Flock {

    constructor(n) {
        this.Boids = [];
        for (let i = 0; i < n; i++) {
            this.Boids.push(new Boid());
        }
    }

    show() {
        for ( let boid of this.Boids ) {
            boid.show();
        }
    }

    cohort() {
        for ( let boid of this.Boids ) {
            boid.cohort(this.Boids);
        }
    }

    update() {
        for ( let boid of this.Boids ) {
            boid.update();
        }
    }

    edges() {
        for ( let boid of this.Boids ) {
            boid.edges();
        }
    }

}
