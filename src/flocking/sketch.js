let flock;

let params = {
  align: 1.5,
  alignMin: 0,
  alignMax: 5,
  alignStep: 0.1,
  //
  cohesion: 1,
  cohesionMin: 0,
  cohesionMax: 5,
  cohesionStep: 0.1,
  //
  separation: 2,
  separationMin: 0,
  separationMax: 5,
  separationStep: 0.1
}

let _gui;

function setup() {
  createCanvas(1080, 1080);
  
  gui();

  flock = new Flock(200);
}

function gui() {
  _gui = createGui('Flock Simulator');
  _gui.addObject(params);
}

function draw() {
  background(51);

  flock.show();
  flock.edges();
  flock.cohort();
  flock.update();
}