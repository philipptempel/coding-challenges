class Particle {
  constructor(pos, vel, acc) {
    this.home = pos.copy();
    this.position = pos;
    this.velocity = vel || createVector(0, 0);
    this.acceleration = acc || createVector(0, 0);
    this.mass = 1;
    this.radius = 5;
    this.maxVelocity = 10;
    this.maxForce = 2;
    this.attractRadius = 100;
    this.fleeRadius = 100;
  }

  get hx() {
    return this.home.x;
  }

  get hy() {
    return this.home.y;
  }

  get x() {
    return this.position.x;
  }

  get y() {
    return this.position.y;
  }

  get force() {
    return this.acceleration.copy().mult(this.mass);
  }

  get heading() {
    return this.velocity.heading();
  }

  set x(v) {
    this.position.x = v;
  }

  set y(v) {
    this.position.y = v;
  }

  set force(v) {
    this.acceleration.add(v.copy().div(this.mass).limit(this.maxForce));
  }

  edges() {
    this.x = max(min(width, this.x), 0);
    this.y = max(min(height, this.y), 0);
  }

  behaviors() {
    let pmouse = createVector(mouseX, mouseY);

    this.force = p5.Vector.add(
      this
      .flee(pmouse)
      .mult(params.flee),
      this
      .attract(this.home)
      .mult(params.attract));
  }

  attract(target) {
    var desired = p5.Vector.sub(target, this.position);
    var d = desired.mag();
    
    var speed = this.maxVelocity;
    if (d < this.attractRadius) {
      speed = map(d **(4/5), 0, this.attractRadius, 0, this.maxVelocity);
    }
    
    return p5.Vector.sub(desired.setMag(speed), this.velocity).limit(this.maxForce);
  }

  flee(target) {
    let desired = p5.Vector.sub(target, this.position);

    if (desired.mag() < this.fleeRadius) {
      return p5.Vector
        .sub(
          desired
          .setMag(this.maxVelocity)
          .mult(-1),
          this.velocity)
        .limit(this.maxForce);
    } else {
      return createVector(0, 0);
    }
  }

  update() {
    this.position
      .add(this.velocity);
    this.velocity
      .add(this.acceleration);
    this.velocity
      .limit(this.maxVelocity);

    this.edges();

    this.acceleration
      .mult(0);
  }

  draw() {
    push();
    translate(this.x, this.y);
    stroke(255);
    strokeWeight(this.radius);
    point(0, 0);
    pop();
  }

}