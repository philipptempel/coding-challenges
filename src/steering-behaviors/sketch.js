let boid = [];
let mouseRadius;
let previousText = "";
let font;
let fontSize;
let gui;

let params = {
  text: "",
  // Fleeing force
  flee: 5,
  fleeMin: 0.0,
  fleeMax: 5.0,
  fleeStep: 0.1,
  // Attraction force
  attract: 1,
  attractMin: 0.0,
  attractMax: 5.0,
  attractStep: 0.1,
  // Sample factor
  sampleFactor: 0.15,
  sampleFactorMin: 0.01,
  sampleFactorMax: 1.00,
  sampleFactorStep: 0.005
}

let oldParams = {
  text: "",
  sampleFactor: 0.15
}

function preload() {
  font = loadFont('TUDelft-UltraLight.otf');
}

function setup() {
  mouseRadius = 50;
  params.text = "Hello World!";
  fontSize = 64;

  createCanvas(400, 400);

  setupGui();
  init();
}

function setupGui() {
  gui = createGui("Steering");
  gui.addObject(params);
  gui.setPosition(5, height + 5);
  QuickSettings.setGlobalChangeHandler(guiUpdate);
}

function guiUpdate() {
  init();
}

function init() {
  let points = [];
  let t = params.text;
  let sf = params.sampleFactor;
  let fontBounds;

  if (t !== oldParams.text || sf !== oldParams.sampleFactor) {
    // Delete old boids
    boid = [];

    if (t !== "") {

      fontBounds = font.textBounds(t, 0, 0, fontSize);

      if (fontBounds.w > 0.99 * width || fontBounds.h > 0.99 * height) {
        do {
          fontBounds = font.textBounds(t, 0, 0, --fontSize);
        } while (
          fontBounds.w > 0.99 * width || fontBounds.h > 0.99 * height
        );
      } else if (fontBounds.w < 0.95 * width && fontBounds.h < 0.95 * height) {
        do {
          fontBounds = font.textBounds(t, 0, 0, ++fontSize);
        } while (fontBounds.w < 0.95 * width && fontBounds.h < 0.95 * height);
      }

      points = font.textToPoints(t, width / 2 - (fontBounds.x + fontBounds.w / 2), height / 2 - (fontBounds.y + fontBounds.h / 2), fontSize, {
        sampleFactor: sf
        // simplifyThreshold: 0.1
      });
    }

    oldParams.text = t;
    oldParams.sampleFactor = sf;
  }

  if (points.length) {
    // Create particles for each point
    let p;
    for (let pnt of points) {
      p = new Particle(createVector(pnt.x, pnt.y));
      p.radius = log(fontSize) /log(2);
      boid.push(p);
    }
  }
}

function draw() {
  background(31);

  for (let particle of boid) {
    particle.draw();
  }

  for (let particle of boid) {
    particle.behaviors();
  }

  for (let particle of boid) {
    particle.update();
  }
}

function bbox(pts) {
  let bbx = [];
  let minx = Infinity,
    maxx = -Infinity,
    miny = Infinity,
    maxy = -Infinity;

  for (let pnt of pts) {
    minx = min(minx, pnt.x);
    maxx = max(maxx, pnt.x);
    miny = min(miny, pnt.y);
    maxy = max(maxy, pnt.y);
  }

  bbx = {
    'x': (maxx + minx) / 2,
    'y': (maxy + miny) / 2,
    'w': (maxx - minx) / 2,
    'h': (maxy - miny) / 2
  }

  return bbx;
}
