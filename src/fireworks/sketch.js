let fireworks = [];

let gravity;

function setup() {
  createCanvas(1024, 768);
  
  gravity = createVector(0, 0.2);
  fireworks.push(new Firework(createVector(width/2, height)));
}

function mousePressed() {
  fireworks.push(new Firework(createVector(mouseX, height)));
}

function draw() {
  background(67);
  
  for (var i = fireworks.length - 1; i >= 0; i--) {
    fireworks[i].draw();
    fireworks[i].applyForce(gravity);
    fireworks[i].update();

    if (fireworks[i].done()) {
      fireworks.splice(i, 1);
    }
  }
  
  if ( random() < 0.3 ) {
    fireworks.push(new Firework(createVector(random(0.1 * width, 0.9 * width), height)))
  }

}