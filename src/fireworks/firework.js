class Firework {
  constructor(pos) {
    this.exploded = false;
    this.particles = [];
    this.particles.push(new Particle(pos, p5.Vector.fromAngle(PI/2 + random(-PI/12, PI/12)).setMag(random(-16, -8))));
  }
  
  applyForce(f) {
    for ( let particle of this.particles ) {
      particle.applyForce(f);
    }
  }
  
  draw() {
    for ( let particle of this.particles ) {
      particle.draw();
    }
  }
  
  done() {
    let flag = true;
    
    for ( let particle of this.particles )  {
      flag = flag && particle.done();
    }
    
    return flag;
  }
  
  update() {
    for (var i = this.particles.length - 1; i >= 0; i--) {
      // If particle is invisible...
      if ( this.particles[i].done() ) {
        // ... remove it
        this.particles.splice(i, 1);
      
      // Particle visible, so ...
      } else {
        // Update its motion
        this.particles[i].update();
        // And reduce its visibility
        if ( this.exploded ) {
          this.particles[i].alpha -= random(4, 10);
        }
      }
    }
    this.explode();
  }
  
  explode() {
    if ( ! this.exploded ) {
      let firework = this.particles[0];
      if ( firework.velocity.y > 0 ) {
        this.particles = [];
        
        let pos = firework.position;
        
        for ( let i = 0; i < random(10, 100); i++) {
          let particle = new Particle(pos.copy(), p5.Vector.random2D().setMag(random(4, 12)));
          
          particle.size = firework.size / 2;
          particle.hue = firework.hue;
          
          this.particles.push(particle);
          
        }
        
        this.exploded = true;
      }
    }
  }
}
