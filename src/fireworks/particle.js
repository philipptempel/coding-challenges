class Particle {
  constructor(pos, vel) {
    this.position = pos;
    this.velocity = vel ? vel : createVector(0, 0);
    this.acceleration = createVector(0, 0);
    this.size = 6;
    this.hue = random(255);
    this.alpha = 255;
  }
  
  applyForce(f) {
    this.acceleration.add(f);
  }
  
  update() {
    this.position.add(this.velocity);
    this.velocity.add(this.acceleration);
    
    this.acceleration.mult(0);
  }
  
  done() {
    return this.alpha < 0;
  }
  
  draw() {
    translate(this.position.x, this.position.y);
    
    colorMode(HSB);
    stroke(this.hue, 255, 255, this.alpha / 255);
    strokeWeight(this.size);

    let r = this.size;
    
    point(0, 0);
    
    resetMatrix();
    colorMode(RGB);
  }
}
