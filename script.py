#!/usr/bin/env python3
import json
import pathlib as pl
import shutil
from typing import List




class Project(object):
    src: pl.Path
    public: pl.Path
    config: dict

    def __init__(self, path: pl.Path):
        self.src = path.resolve()
        self.public = (path / '..' / '..' / 'public' / self.src.name).resolve()
        self.config = self._load_config()

    @property
    def projectfile(self):
        return self.src / 'project.json'

    def publish(self):
        shutil.copy2(self.src, self.public)

    def _load_config(self):
        try:
            with self.projectfile.open() as f:
                return json.load(f)
        except:
            return {}


class Collection(object):
    def __init__(self, projects: List[Project]):
        self.projects = projects

    def publish(self):
        for project in self.projects:
            project.publish();
        self.make_index()


def main():
    src = pl.Path() / 'src'

    ps = src.glob('*')

    cs = []

    for p in ps:
        # skip assets directory
        if p.name == "assets" or not p.is_dir():
            continue

        # project configuration file name
        c = p / 'project.json'

        # load JSON file if it exists
        try:
            with c.open() as f:
                d = json.load(f)
                d['src'] = p.name
                cs.append(d)
        # JSON file does not exist, so skip project i.e., don't publish it
        except:
            continue

    # sort projects by name
    cs = sorted(cs, key=lambda x: x['title'].lower())

    cards = []

    for c in cs:
        card  = f"      <div class=\"col-sm-12 col-md-6 col-lg-4 mb-4\">\n"
        card += f"        <div class=\"card\">\n"
        card += f"          <div class=\"card-header\">\n"
        card += f"            <h3 class=\"card-title\">\n"
        card += f"              {c.get('title', c['name'])}\n"
        card += f"            </h3>\n"
        card += f"          </div>\n"
        if c.get('description', ''):
          card += f"          <div class=\"card-body\">\n"
          card += f"            <p class=\"card-text\">{c.get('description', '')}</p>\n"
          card += f"          </div>\n"
        card += f"          <div class=\"card-body small text-end\">\n"
        card += f"            <a href=\"{ c.get('src','') }/index.html\" class=\"btn btn-primary btn-sm stretched-link\">View</a>\n"
        card += f"          </div>\n"
        card += f"        </div>\n"
        card += f"      </div>\n"
        cards.append(card)

    html = '''<!doctype html>
<html lang="en" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Philipp tempel">
  <title>Coding Challenges</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
  <link href="assets/css/sticky-footer.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">

<!-- Begin page content -->
<main class="flex-shrink-0">
  <div class="container">
    <h1 class="mt-5">Coding Challenges</h1>
    <p class="lead">
      A collection of my coding challenges in <a href="http://p5js.org">p5.js</a> inspired by <a href="http://thecodingtrain.com/">The Coding Train</a>.
    </p>

    <div class="row" data-masonry='{"percentPosition": true }'>
'''
    html += "".join(cards)
    html += """
    </div>
  </div>

</main>

<footer class="footer mt-auto py-3 bg-light">
<div class="container">
  <span class="text-muted">&copy;2021&nbsp;Philipp Tempel.</span>
</div>
</footer>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
</body>
</html>
"""

    index = src / 'index.html'

    index.write_text(html)


if __name__ == "__main__":
    main()
